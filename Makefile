make docker-prod:
	docker run -d -it --name admin-web -p 5000:5000 admin-web

# make docker-stage:
# 	docker run -d -it --name admin-web -p 5001:5001 admin-web

make docker-build:
	docker build . -t admin-web