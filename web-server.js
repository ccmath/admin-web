import * as path from 'path'
import express from 'express'

import { dirname } from 'path';
import { fileURLToPath } from 'url';

const __dirname = dirname(fileURLToPath(import.meta.url));

const app = express()
const PORT = process.env.PORT || 5000

app.use(express.static(path.resolve(__dirname,"dist")))

app.get("*",(req,res)=>{
    res.sendFile(path.resolve(__dirname,"dist","index.html"))
})

app.listen(PORT,()=>{
    console.log(`listening: ${PORT}`)
})